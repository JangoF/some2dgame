﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

public class ButtonScript: MonoBehaviour {

    public Sprite spriteClosed = null;
    public Sprite spriteOpened = null;

    private MainScript delegateScript = null;

    public static GameObject createCard(
            MainScript mainScriptParent,
            string objectName,
            Sprite spriteOpened,
            Sprite spriteClosed,
            AnimationClip animationOpen,
            AnimationClip animationClose,
            AnimationClip animationRemove
        ) {
        GameObject card = new GameObject(objectName, typeof(Image), typeof(Button), typeof(Animation), typeof(ButtonScript));

        card.GetComponent<Button>().onClick.AddListener(delegate { card.GetComponent<ButtonScript>().didTapButton(); });
        card.GetComponent<Image>().sprite = spriteClosed;

        card.GetComponent<Animation>().AddClip(animationOpen, "card_open");
        card.GetComponent<Animation>().AddClip(animationClose, "card_close");
        card.GetComponent<Animation>().AddClip(animationRemove, "card_remove");

        card.GetComponent<ButtonScript>().spriteClosed = spriteClosed;
        card.GetComponent<ButtonScript>().spriteOpened = spriteOpened;
        card.GetComponent<ButtonScript>().delegateScript = mainScriptParent;

        return card;
    }

    // Use this for initialization
    void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	}

    public void cardShow() {
        GetComponent<Button>().interactable = true;

        Animation animation = GetComponent<Animation>();
        animation["card_remove"].speed = -1;
        animation["card_remove"].time = animation["card_remove"].length;
        animation.Play("card_remove");
    }

    public void cardHide() {
        GetComponent<Button>().interactable = false;

        Animation animation = GetComponent<Animation>();
        animation["card_remove"].speed = 1;
        animation["card_remove"].time = 0;
        animation.Play("card_remove");
    }

    public void cardOpen() {
        GetComponent<Animation>().Play("card_open");
    }

    public void cardClose() {
        GetComponent<Animation>().Play("card_close");
    }

    public void didCardReverse() {
        GetComponent<Image>().sprite = GetComponent<Image>().sprite == spriteClosed ? spriteOpened : spriteClosed;
    }

    public void didCardClosed() {
        delegateScript.didCardClosed();
    }

    public void didCardOpened() {
        delegateScript.didCardOpened();
    }

    public void didTapButton() {
        delegateScript.didTapCard(gameObject);
    }
}
