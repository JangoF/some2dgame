﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

public class MainScript: MonoBehaviour {

    public GameObject panelCardContained = null;

    public Sprite[] cardSpriteOpened;
    public Sprite   cardSpriteClosed;

    public AnimationClip cardAnimationOpen;
    public AnimationClip cardAnimationClose;
    public AnimationClip cardAnimationRemove;

    public int cardScopeValid;
    public int cardScopeInvalid;

    public GameObject finishMessage;
    public Text scopeText;

    public uint cardRowCount = 5;
    private int scopeValue = 0;

    private GameObject selectedCardFrist = null;
    private GameObject selectedCardSecond = null;

    private bool isCurrentlyAnimation = false;

    // Use this for initialization
    void Start() {
        generateCardGrid();
    }

    // Update is called once per frame
    void Update() {
    }

    public void generateCardGrid() {
        panelCardContained.GetComponent<GridLayoutGroup>().constraintCount = (int)cardRowCount;

        Sprite[] cardList = new Sprite[cardRowCount * cardRowCount - (cardRowCount % 2 == 1 ? 1 : 0)];

        for (uint i = 0; i < cardList.Length / 2; i++) {
            Sprite currentSprite = cardSpriteOpened[Random.Range(0, cardSpriteOpened.Length)];
            cardList[i * 2] = currentSprite;
            cardList[i * 2 + 1] = currentSprite;
        }

        Sprite elementTemp;
        for (uint i = 0; i < cardList.Length; i++) {
            uint randomPosition = (uint)Random.Range(0, cardList.Length);

            elementTemp = cardList[randomPosition];
            cardList[randomPosition] = cardList[i];
            cardList[i] = elementTemp;
        }

        int index = cardRowCount % 2 == 1 ? (int)(cardList.Length / 2) : -1;

        for (uint i = 0; i < cardList.Length; i++) {
            if (i == index) {
                GameObject empty = new GameObject("empty", typeof(Image), typeof(Button));
                empty.GetComponent<Button>().interactable = false;
                empty.GetComponent<Image>().enabled = false;
                empty.transform.SetParent(panelCardContained.transform);
            }

            GameObject card = ButtonScript.createCard(
                this,
                i.ToString(),
                cardList[i],
                cardSpriteClosed,
                cardAnimationOpen,
                cardAnimationClose,
                cardAnimationRemove
            );

            card.transform.SetParent(panelCardContained.transform);
            card.GetComponent<ButtonScript>().cardShow();
        }
    }

    public void resetAll() {
        selectedCardFrist = null;
        selectedCardSecond = null;

        scopeText.text = "0";
        scopeValue = 0;

        isCurrentlyAnimation = false;
        foreach (Transform child in panelCardContained.transform) {
            GameObject.Destroy(child.gameObject);
        }

        finishMessage.GetComponent<Animation>().Play("FinishMessageHide");
    }

    bool isAllCardsDropped() {
        foreach (Transform child in panelCardContained.transform) {
            if (child.GetComponent<Button>().interactable == true) {
                return false;
            }
        }

        return true;
    }

    public void didTapCard(GameObject currentCard) {
        if (isCurrentlyAnimation == true) {
            return;
        }
        
        currentCard.GetComponent<ButtonScript>().cardOpen();
        isCurrentlyAnimation = true;

        if (selectedCardFrist == null) {
            selectedCardFrist = currentCard;
        } else {
            selectedCardSecond = currentCard;
        }
    }

    public void didCardOpened() {
        if (selectedCardSecond != null) {
            if (selectedCardFrist.GetComponent<ButtonScript>().spriteOpened == selectedCardSecond.GetComponent<ButtonScript>().spriteOpened) {
                selectedCardFrist.GetComponent<ButtonScript>().cardHide();
                selectedCardSecond.GetComponent<ButtonScript>().cardHide();

                selectedCardFrist = null;
                selectedCardSecond = null;

                scopeValue += cardScopeValid;
                scopeText.text = scopeValue.ToString();

                if (isAllCardsDropped() == true) {
                    finishMessage.GetComponent<Animation>().Play();
                    GetComponent<AudioSource>().Play();
                }

            } else {
                selectedCardFrist.GetComponent<ButtonScript>().cardClose();
                selectedCardSecond.GetComponent<ButtonScript>().cardClose();

                selectedCardFrist = null;
                selectedCardSecond = null;

                scopeValue += cardScopeInvalid;
                scopeText.text = scopeValue.ToString();

                isCurrentlyAnimation = true;
                return;
            }
        }

        isCurrentlyAnimation = false;
    }

    public void didCardClosed() {
        isCurrentlyAnimation = false;
    }
}